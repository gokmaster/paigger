<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'paigger\\' => array($baseDir . '/paigger'),
    'gpApp\\' => array($baseDir . '/gpApp'),
);
