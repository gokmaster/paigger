# Paigger

A PHP web application that allows users to create articles and then share with the groups they belong to, with everyone, or with yourself only.


# How to run this web application on your server?
1. Place the contents of the "paigger" folder in the root directory of your server such as in "htdocs" folder if your running XAMPP, or in "www" folder if your running WAMP.

2. Import the sql file located in the "db" folder to your datebase.

3. Modify database credentials in "paigger\class\gpApp\db\dbConn.php".

4. Edit variable $subDir in "paigger\class\paigger\variable\url.php" if necessary.
